package dms


import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(EncounterController)
class EncounterControllerTests {

    void testSomething() {
        fail "Implement me"
    }
}
