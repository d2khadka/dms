package dms

class Hospital {

    String hospitalName
    String address
    String phone

    static hasMany = [encounter:Encounter,doctors:Doctor]


    static constraints = {
        hospitalName(nullable: false)
        address(nullable: false)
        phone(nullable: false)
    }

    String toString() {
        "${hospitalName}"
    }
}
