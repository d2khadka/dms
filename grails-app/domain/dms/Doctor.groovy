package dms

class Doctor {

    String doctorId
    String fullName
    String specialist
    String phone


    static hasMany = [encounter:Encounter,hospitals:Hospital]

    static belongsTo = Hospital

    static constraints = {
        doctorId(unique: true ,nullable: false)
        fullName(nullable: false,size:4..15)
        specialist(inList: ["pedo","dermo"])
        phone()

    }

    String toString() {
        "${doctorId}"
    }
}
