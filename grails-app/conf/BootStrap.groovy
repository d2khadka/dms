import dms.Doctor
import dms.Encounter
import dms.Hospital

class BootStrap {

    def init = { servletContext ->
        Doctor dh=new Doctor(doctorId:"1",fullName:"dhiraj khadka",specialist: "pedo",phone: "9849224585" ).save()
        Doctor la=new Doctor(doctorId:"2",fullName:"lame jame",specialist: "pedo",phone: "984924585" ).save()
        Doctor its=new Doctor(doctorId:"3",fullName:"its sanw",specialist: "dermo",phone: "9849224585" ).save()

        Hospital h1=new Hospital(hospitalName: "mem",address:"lame",phone: "123123321",doctors: [dh,la] ).save()
        Hospital h2=new Hospital(hospitalName: "h2",address:"lame",phone: "123123321" ).save()

        Encounter e1=new Encounter(date:new Date(1992,10,25),hospital: h1,doctor: dh).save()
        Encounter e2=new Encounter(date:new Date(2002,3,4),hospital: h2,doctor: la).save()

        println("initializing data")
    }
    def destroy = {
    }
}
